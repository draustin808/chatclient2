#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <time.h>
#include <sys/select.h>
#include <sys/time.h>
#include <ncurses.h>
#include <curses.h>
#include <pthread.h>

#define SERVER "10.115.20.250"
#define PORT 49155
#define BUFSIZE 1024
#define WHT   "\x1B[37m"
#define RESET "\x1B[0m"

int fd,len;
WINDOW * lgWindow;
WINDOW * smWindow;
WINDOW * mdWindow;
WINDOW * sendWindow;


//used the starter code supplied by John Rodkey
//worked on with Matt Beall recieved help from Ryan on the multithreading and ncurses.

int connect2v4stream(char * srv, int port) {
	int ret,sockd;
	struct sockaddr_in sin;

	if ( (sockd = socket(AF_INET, SOCK_STREAM, 0)) == -1){
		printf("ERROR: Error creating socket. errno = %d\n", errno);
		exit(errno);
	}
	if ( (ret = inet_pton(AF_INET, SERVER, &sin.sin_addr)) <= 0){
		printf("ERROR: trouble converting using inet_pton. \
			return value = %d, errno = %d\n", ret, errno);
		exit(errno);
	}

	sin.sin_family = AF_INET;  //IPv4
	sin.sin_port = htons(PORT); //Convert port to network endian

	if ( (connect(sockd, (struct sockaddr *) &sin, sizeof(sin))) == -1){
		printf("ERROR: trouble connecting to server. errno = %d\n", errno);
		exit(errno);
	}

	return sockd;
}

int sendout(int fd, char *msg){
	int ret;
	ret = send(fd, msg, strlen(msg), 0);
	if ( ret == 1 ){
		printf("ERROR: trouble sending. errno = %d\n", errno);
		exit(errno);
	}

	return strlen(msg);
}

void recvandprint (int fd, char *buff){
	int ret;

	for(;;){
		buff = malloc(BUFSIZE+1);
		ret = recv(fd, buff, BUFSIZE, 0);
		if( ret == -1 ){
			if( errno == EAGAIN ){
				break;
			}else{
				printf("ERROR: error receiving. errno = %d\n", errno);
				exit(errno);
			}
		}else if( ret == 0){
			exit(0);
		}else{
			buff[ret] = 0;
			
			cbreak();
			wprintw(smWindow, buff);
			wrefresh(smWindow);

		}

		free(buff);
	}
}

void* wthread (void* param){
	char *buffer, *origbuffer;
	int is_done = 0;
	while( ! is_done ){
		recvandprint(fd,buffer); //print any input

		len = BUFSIZE;
		buffer = malloc(len+1);
		origbuffer = buffer;
		
		wgetstr(mdWindow, buffer);
		strcat(buffer, "\n");
		wrefresh(mdWindow);

		sendout(fd, buffer);

		is_done = (strcmp (buffer, "quit\n") == 0);
		free(origbuffer);
	}

}

void* readthread(void* param){
	for(;;){
		char* b;
		recvandprint(fd, b);
	}
}

void windowMaker(){
	initscr();

	int height = 30;
	int width = 50;

	//this allocates the sizing for our windows
	lgWindow = newwin(height, width, 1, 1);
	smWindow = newwin(height - 2, width - 2, 2, 2);
	refresh();

	scrollok(lgWindow, TRUE);
	scrollok(smWindow, TRUE);

	wrefresh(lgWindow);
	wrefresh(smWindow);
	refresh();

	box(lgWindow,0,0);
	wrefresh(lgWindow);



	mdWindow = newwin(height, width, 1, width+1);
	sendWindow = newwin(height, width, 2, width+2);
	refresh();

	scrollok(mdWindow, TRUE);
	scrollok(sendWindow, TRUE);

	wrefresh(mdWindow);
	wrefresh(sendWindow);
	refresh();

	box(mdWindow,0,0);
	wrefresh(mdWindow);

}


int main(int argc, char * argv[]) {
	char *name, *buffer, *origbuffer;
	struct timeval timev;

	
	
	

	pthread_t multiRead;
	pthread_t multiSend;

	fd = connect2v4stream( SERVER, PORT);

	//Setup recv timeout for .5 seconds
	timev.tv_sec = 0;
	timev.tv_usec = 1000 * 500;
	setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, &timev, sizeof(timev));

	//Set name based on arguments
	if(argc < 2 ){
		printf(WHT "Usage: chat-client <screenname\n" RESET);
		exit(1);
	}

	name = argv[1];
	len = strlen(name);
	name[len] = '\n'; //insert \n
	name[len+1] = '\0'; //reterminate the string
	sendout(fd, name);

	initscr();
	windowMaker();

	pthread_create(&multiRead, NULL, readthread, NULL);
	pthread_create(&multiSend, NULL, wthread, NULL);

	pthread_join(multiRead, NULL);
	pthread_join(multiSend, NULL);


	endwin();

	
}

